## Test application for AppSmart

## Steps

-   make model, factory, seeder for Product
-   make config and language files
-   make repository for openfood api
-   make layout and templates for page
-   make Controllers
-   write routes
-   test Application by PHPUnit
