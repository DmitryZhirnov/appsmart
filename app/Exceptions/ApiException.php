<?php

namespace App\Exceptions;

use Exception;

class ApiException extends Exception
{
    public function __construct($message = 'Application Api Exception', $code = 499)
    {
        $this->message = $message;
        $this->code = $code;
    }
}
