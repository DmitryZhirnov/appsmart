<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Services\OpenFoodApiRepository;
use App\Services\Paginator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class OpenFoodApiController extends Controller
{
    /**
     * Show products with paginate
     * @param OpenFoodApiRepository - repository for working with https://world.openfoodfacts.org/
     * @param Request - 
     */
    public function index(OpenFoodApiRepository $repository, Request $request)
    {
        try {
            $result = Cache::remember(
                "{$request->page}_{$request->search}",
                Carbon::now()->addHour(),
                function () use ($request, $repository) {
                    $response = $repository->get($request);
                    return $response;
                }
            );
        } catch (ApiException $ex) {
            return Session::flash('store-error', trans('app.store-error'));
            report($ex);
        }
        $nextLink = Paginator::getNextLink($result, $request->search);
        $prevLink = Paginator::getPrevLink($result, $request->search);

        return view(
            'product.index',
            [
                'products' => $result->products,
                'next' => $nextLink,
                'prev' => $prevLink
            ]
        );
    }
}
