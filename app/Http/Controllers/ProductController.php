<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * @param ProductRequest $request - request for creating or updating product to DB
     * @return RedirectResponse - redirect back with success or error
     */
    public function store(ProductRequest $request)
    {
        try {
            Product::updateOrCreate($request->except('_token'));
            Session::flash('store-success', trans('app.store-success'));

            return Redirect::back();
        } catch (\Exception $ex) {

            Session::flash('store-error', trans('app.store-error'));
            info($ex->getMessage());

            return Redirect::back();
        }
    }
}
