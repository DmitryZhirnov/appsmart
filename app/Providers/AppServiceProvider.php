<?php

namespace App\Providers;

use App\Services\ApiRepository;
use Illuminate\Support\ServiceProvider;
use App\Services\OpenFoodApiRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OpenFoodApiRepository::class, function () {
            $api = new OpenFoodApiRepository();
            $api->setApiUrl(config('api.search_url'));
            $api->setApiError(trans('app.api-get-data-error'));
            return $api;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
