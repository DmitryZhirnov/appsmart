<?php

namespace App\Services;

use App\Exceptions\ApiException;
use Illuminate\Http\Request;

class OpenFoodApiRepository
{
    private $searchUrl;
    private $apiErrorMessage;

    public function setApiUrl($searchUrl)
    {
        $this->searchUrl = $searchUrl;
    }

    public function setApiError($apiErrorMessage)
    {
        $this->apiErrorMessage = $apiErrorMessage;
    }
    /**
     * Get list product searching by query
     * @param Request $request (page, search)  
     */
    public function get(Request $request)
    {
        $page = $request->page ?? 1;
        $search = $request->search ?? '';
        $perPage = $request->per_page ?? '';

        $response = $this->getData($page, $search, $perPage);
        return $this->prepare($response);
    }

    /**
     * Get data from api by curl
     * @param $page - current page
     * @param $search - searchable word
     */
    private function getData($page, $search, $perPage)
    {
        $curl = curl_init();
        $params = $this->makeRequestArray($search, $page, $perPage);
        $url = $this->searchUrl . '?' . http_build_query($params);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new ApiException();
        } else {
            return $response;
        }
    }

    /**
     * Make response smallest, return only needable fields
     * @param $response - response with all properties
     * @return stdClass  
     */
    private function prepare($response)
    {
        $result = json_decode($response);

        $products = array_map(function ($product) {
            return [
                'code' => $product->code ?? '',
                'image_url' => $product->image_url ?? '',
                'product_name' => $product->product_name ?? '',
                'categories' => $product->categories ?? ''
            ];
        }, $result->products);

        $result->products = $products;
        return $result;
    }

    /**
     * Make request array for building url
     */

    private function makeRequestArray($search, $page, $perPage)
    {
        return array(
            'search_terms' => $search,
            'search_simple' => '1',
            'action' => 'process',
            'json' => '1',
            'page_size' => $perPage,
            'page' => $page
        );
    }
}
