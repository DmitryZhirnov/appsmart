<?php

namespace App\Services;

class Paginator
{

    /**
     * Make next link for pagination
     */
    public static function getNextLink($result, $search)
    {
        $nextPage = self::getNextPage($result);
        if ($nextPage === 0) {
            return "";
        }

        return self::buildUrl('/', ['page' => $nextPage, 'search' => $search]);
    }

    /**
     * Make prev link for pagination
     */
    public static function getPrevLink($result, $search)
    {
        $prevPage = self::getPrevPage($result);
        if ($prevPage === 0) {
            return "";
        }

        return self::buildUrl('/', ['page' => $prevPage, 'search' => $search]);
    }

    /**
     * Get next page
     */
    public static function getNextPage($result)
    {
        return $result->count <= $result->page * $result->page_size ? 0 : $result->page + 1;
    }

    /**
     * Get prev page
     */
    public static function getPrevPage($result)
    {
        return $result->page == 1 ? 0 : $result->page - 1;
    }

    public static function buildUrl($url, $params)
    {
        return url('/') . '?' . http_build_query($params);
    }
}
