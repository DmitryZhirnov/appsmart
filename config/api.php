<?php

return [
    'search_url' => 'https://world.openfoodfacts.org/cgi/search.pl',
    'per_page' => 5
];
