<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;
    protected $categories = [
        "Boissons, Eaux, Eaux de sources, Eaux minérales, Boissons sans sucre ajouté",
        "Snacks, Snacks sucrés, Biscuits et gâteaux, Biscuits, Biscuits fourrés",
        "Beverages, Carbonated drinks, Sodas, Colas, Sweetened beverages, Boissons-boissons-gazeuses-sodas-boissons-sans-alcool-sodas-au-cola-boissons-avec-sucre-ajoute-pl-zawiera-kofeinę"
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->numerify("#############"),
            'image_url' => $this->faker->imageUrl(),
            'product_name' => $this->faker->lexify("???? ???? ????????"),
            'categories' => $this->categories[rand(0, 2)]
        ];
    }
}
