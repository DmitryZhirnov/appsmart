<?php
// Application messages and string variables

return [
    'image' => 'Product image',
    'product_name' => 'Product Name',
    'categories' => 'Product Categories',
    'add-button' => 'Add New Product',
    'search-button' => 'Search Products',
    'add-edit-button' => 'Add/Edit',
    'code' => 'Product Code',
    'tools' => 'Tools',
    'store-success' => 'Product update/create successfully',
    'store-error' => 'Product not updated/created. Something went wrong!'
];
