@extends('layout.app')

@section('content')
@include('product.search-form')

@if(Session::get('store-error'))
<div class="alert alert-danger">
    {{Session::get('store-error')}}
</div>
@endif

@if(Session::get('store-success'))
<div class="alert alert-success">
    {{Session::get('store-success')}}
</div>
@endif

@if(Session::get('errors'))
<div class="alert alert-danger">    
@foreach ($errors->all() as $error)
    {{$error}}<br/>
@endforeach
</div>    
@endif


<table class="table table-striped" >
    <thead class="thead-dark">
        <tr>
        <th>@lang('app.tools')</th>
        <th>@lang('app.code')</th>
        <th>@lang('app.image')</th>
        <th>@lang('app.product_name')</th>
        <th>@lang('app.categories')</th>
        </tr>
    </thead>
    <tbody>

        @foreach($products as $product)
        <form action="/product" method="POST">
            @csrf
            <input type="hidden" name="code" value="{{$product['code']}}">
            <input type="hidden" name="image_url" value="{{$product['image_url']}}">
            <tr>
            <td><button type="submit" value="{{$product['code']}}" class="btn btn-small btn-link">@lang('app.add-edit-button')</button></td>
                <td>{{$product["code"]}}</td>
                <td>
                    <img src="{{$product["image_url"]}}" alt="{{$product["product_name"]}}" height="100px">
                </td>
                <td><textarea name="product_name" value="{{$product['product_name']}}" class="form-control">{{$product['product_name']}}</textarea></td>
                <td><textarea name="categories" class="form-control">{{$product["categories"]}}</textarea></td>
            </tr>
        </form>
        @endforeach

    </tbody>
</table>
<a href="{{$prev}}" class="btn btn-default border">@lang('pagination.previous')</a>
<a href="{{$next}}" class="btn btn-default border">@lang('pagination.next')</a>
@endsection