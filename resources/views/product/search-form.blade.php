<form method="GET" action="/" id="search-form">
    @csrf
    <div class="form-group">
        <label for="search">@lang('app.product_name')</label>
        <input id="search" name="search" type="text" class="form-control">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg">
            @lang('app.search-button')
        </button>
    </div>
</form>