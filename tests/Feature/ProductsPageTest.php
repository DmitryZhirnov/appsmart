<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsPageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testProductsSeeAppName()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee(config("app.name"));
    }

    public function testProductsTableSeeTableHeaders()
    {
        $response = $this->get('/');

        $response->assertSeeText(trans('app.image'));
        $response->assertSeeText(trans('app.product_name'));
        $response->assertSeeText(trans('app.categories'));
    }

    public function testSeeSearchForm()
    {
        $response = $this->get('/');
        $response->assertSee('search-form');
    }

    public function testSeeSearchButton()
    {
        $response = $this->get('/');
        $response->assertSee(trans('app.search-button'));
    }
}
