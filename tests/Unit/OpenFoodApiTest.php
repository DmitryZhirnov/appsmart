<?php

namespace Tests\Unit;

use App\Services\OpenFoodApiRepository;
use Illuminate\Translation\Translator;
use Mockery;
use PHPUnit\Framework\TestCase;
use stdClass;


class OpenFoodApiTest extends TestCase
{
    private $api;

    public function setUp(): void
    {
        $this->api = app()->make(OpenFoodApiRepository::class);
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function testMakeRequestArray()
    {
        $requestArray = array(
            'search_terms' => 'search',
            'search_simple' => '1',
            'action' => 'process',
            'json' => '1',
            'page_size' => 10,
            'page' => '1'
        );

        $actualArray = PHPUnitUtil::callMethod(
            $this->api,
            'makeRequestArray',
            ['search' => 'search', 'page' => '1', 'perPage' => '10']
        );

        $this->assertEquals($requestArray, $actualArray);
    }

    public function testPrepareResponse()
    {
        $object = new stdClass();
        $product = new stdClass();

        $product->code = '123123123';
        $product->image_url = 'image_url';
        $product->product_name = 'Coca Cola';
        $product->categories = 'Product categories';
        $product->someValue = 'Some Value';

        $object->page = 2;
        $object->page_size = 5;
        $object->count = 2110;
        $object->products = [$product];

        $json = json_encode($object);

        $result = PHPUnitUtil::callMethod(
            $this->api,
            'prepare',
            ['response' => $json]
        );

        $result = json_encode($result);
        $this->assertNotEquals($json, $result);
        $this->assertStringNotContainsString('Some Value', $result);
    }
}
