<?php

namespace Tests\Unit;

use App\Services\Paginator;
use PHPUnit\Framework\TestCase;
use stdClass;

class PaginatorTest extends TestCase
{
    private $result;

    public function setUp(): void
    {
        $this->result = new stdClass;
        $this->result->page_size = 5;
        $this->result->count = 10;
    }
    public function testGetNextPage()
    {
        $this->result->page = 1;
        $this->assertEquals(Paginator::getNextPage($this->result), 2);

        $this->result->page = 2;
        $this->assertEquals(Paginator::getNextPage($this->result), 0);
    }

    public function testGetPrevPage()
    {
        $this->result->page = 2;
        $this->assertEquals(Paginator::getPrevPage($this->result), 1);

        $this->result->page = 1;
        $this->assertEquals(Paginator::getPrevPage($this->result), 0);
    }
}
